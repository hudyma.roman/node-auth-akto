const multer = require("multer");
const moment = require("moment");
const router = require("express").Router();
const { auth } = require("../middleware/verifyToken");
const { resCraft } = require("../helper/resCraft");
const ProductModel = require("../model/Products");
const fs = require("fs");

//MULTER Files Saver
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./public/img");
  },
  filename: function (req, file, cb) {
    const date = moment().format("DDMMYYYY-HHmmss_SSS");
    cb(null, `${date}-${file.originalname}`);
  },
});

const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === "image/png" ||
    file.mimetype === "image/jpeg" ||
    file.mimetype === "image/svg" ||
    file.mimetype === "image/jpg"
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const limits = {
  fileSize: 1024 * 1024 * 5,
};

const upload = multer({ storage, fileFilter, limits }).single("image");

router.get("/", async (req, res) => {
  let all = await ProductModel.find().populate({
    path: "author",
    select: "name email -_id",
  });
  res.send(resCraft("success", "Take all Products", all));
});
router.delete("/:id", async (req, res) => {
  let item = await ProductModel.findById(req.params.id);
  if (!item) return res.send(resCraft("error", "Product id not fond"));
  let mess = "";
  if (!item.img.title == "no-image.svg") {
    fs.unlink(item.img.path, (err) => {
      if (!err) return (mess = " and Image");
      console.log(err);
    }); // ---- Удаление файла
  }
  let deletedItem = await ProductModel.findByIdAndDelete(req.params.id);
  if (!deletedItem) return res.send(resCraft("error", "Incorect Id Product"));
  res.send(
    resCraft("success", `Product${mess} successfully removed`, deletedItem)
  );
});

router.post("/add", [auth, upload], async (req, res) => {
  try {
    if (!req.body.title || !req.body.count || !req.body.price)
      return res.send(resCraft("error", "Incorect Params"));
    let realPath;
    const item = new ProductModel({
      title: req.body.title,
      count: req.body.count,
      price: req.body.price,
      author: req.user._id,
    });
    if (req.file) {
      let str = process.env.HOST + req.file.path;
      realPath = str.replace(/\\/g, "/"); //  ---- для заміни на повний адрес хосту
      item.img = {
        title: req.file.filename,
        path: req.file.path,
      };
    }

    const savedItem = await item.save();
    res.send(resCraft("success", "Created Product", savedItem));
  } catch (e) {
    res.send(resCraft("error", "Products Error", e));
  }
});

module.exports = router;
