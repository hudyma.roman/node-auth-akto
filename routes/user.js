const router = require("express").Router();
const UsersModel = require("../model/User");
const { auth } = require("../middleware/verifyToken");
const {resCraft} = require("../helper/resCraft");

router.get("/",  async (req, res) => {
  let all = await UsersModel.find().populate({path: 'role', select: 'name -_id'});
  res.send(resCraft('success', 'Success all', all));
});
router.get("/user/all", auth, async (req, res) => {
  let all = await UsersModel.find().populate({path: 'role', select: 'name -_id'})
  res.send(resCraft('success', 'Success all', all));
});

router.put("/user/:id", auth, async (req, res) => {
  //VALID  body.name
  if(!req.body.name) return res.send(resCraft('error', 'Incorrect Params'));

  let rawRes = await UsersModel.findByIdAndUpdate(req.params.id, {name: req.body.name}, {new: true})
  res.send(resCraft('success', 'Update user', rawRes));
});

router.delete("/user/:id", auth, async (req, res) => {
  let user = await UsersModel.findById(req.params.id)
  if(!user) return res.send(resCraft('error', 'User not fond'))

  let rawRes = await UsersModel.findByIdAndDelete(req.params.id)
  res.send(resCraft('success', 'Delete user', rawRes));
});


module.exports = router;
