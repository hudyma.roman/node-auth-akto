const router = require("express").Router();
const { auth } = require("../middleware/verifyToken");
const RoleController = require("../controllers/RoleController");

router.get("/", auth, RoleController.read);
router.post("/", auth, RoleController.create);

module.exports = router;
