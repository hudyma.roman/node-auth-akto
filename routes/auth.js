const router = require("express").Router();
const UsersModel = require("../model/User");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { validRegister, validLogin } = require("../validation");
const RoleModel = require("../model/Role");
const {resCraft} = require("../helper/resCraft");
const { auth } = require("../middleware/verifyToken");


router.post("/register/", async (req, res) => {
  //validate data before create user in DB
  let param = req.query.role;


  const { error } = validRegister(req.body);
  if (error) return res.send(resCraft('error', error?.details[0]?.message));

  // Duplicate Email Validation
  const emailExist = await UsersModel.findOne({ email: req.body.email });
  if (emailExist) return res.send(resCraft('error', "Email already exist"));

  // HASH THE PASSWORD - Шыфровка пароля
  const salt = await bcrypt.genSalt(10);
  const hashPassword = await bcrypt.hash(req.body.password, salt);

  const user = new UsersModel({
    name: req.body.name,
    email: req.body.email,
    password: hashPassword,
  });
  if(param){
    let x = await RoleModel.findOne({name: param});
    user.role = x._id
  }
  try {
    const savedUser = await user.save();
    const detailUser = await UsersModel.findById(savedUser._id).populate({path: 'role', select: 'name -_id'})
    let fullRes = resCraft('success', 'Success Register!', detailUser)
    res.send(fullRes);
  } catch (e) {
    res.send(e);
  }
});

// LOGIN
router.post("/login", async (req, res) => {
  //validate data before create user in DB
  const { error } = validLogin(req.body);
  if (error) return res.send( resCraft('error', error?.details[0]?.message));

  // Duplicate Email Validation
  const user = await UsersModel.findOne({ email: req.body.email });
  if (!user) return res.send( resCraft('error', 'Email don`t exist'));

  // PASSWORD IS CORRECT
  const validPass = await bcrypt.compare(req.body.password, user.password);
  if (!validPass) return res.send(resCraft( 'error', "Invalid Password"));

  //CREATE TOKEN
  const token = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET);
  // res.header("auth-token", token);
  let temp = await UsersModel.findById(user._id).populate({path: 'role', select: 'name -_id'})
  let detailUser = {token, temp}
  let  fullRes = resCraft('success', 'Login Success!', detailUser)
  res.send(fullRes);
});

// GET USER
router.get('', auth, async (req, res) => {
  let x = await UsersModel.findById(req.user._id).populate({path: 'role', select: 'name -_id'})
  let  fullRes = resCraft('success', 'Take user', x)
  res.send(fullRes)
})
module.exports = router;
