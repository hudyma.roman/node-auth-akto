const jwt = require("jsonwebtoken");
const {resCraft} = require("../helper/resCraft");

function auth(req, res, next) {
  const token = req.header("auth-token");
  if (!token) return res.send(resCraft("error", "Access Denied!"));
  try {
    const verified = jwt.verify(token, process.env.TOKEN_SECRET);
    req.user = verified;
    next();
  } catch (e) {
    res.send(resCraft("error", "Invalid Token"));
  }
}

module.exports.auth = auth;
