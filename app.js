require("dotenv").config();
const express = require("express");
const app = express();
const mongoose = require("mongoose");
const PORT = process.env.PORT || 4000;
const cors = require("cors");

mongoose
  .connect(process.env.DB_CONNECT, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("BD test is Conected");
  })
  .catch((e) => {
    console.log(e);
  });

//Import Routes
const authRoute = require("./routes/auth");
const userRoute = require("./routes/user");
const roleRoute = require("./routes/role");
const productsRoute = require("./routes/product");
app.use(cors());
app.use(express.json());
app.use('/public', express.static('public'));
//Route Middleware

app.use('/static', express.static('public'));
app.use("/api/user", authRoute);
app.use("/api/", userRoute);
app.use("/api/role/", roleRoute);
app.use("/api/products/", productsRoute);
app.get('/', (req, res) => {
  res.send('hi from heroku Akto 3.0');
});

app.listen(PORT, () => {
  console.log("App listening on port 3000!");
});
