let resCraft = ( type = 'alert', text = 'Ups! Something went wrong.', main = null ) => {
  let fullResponse = {
    message: {
      type: type,
      text: text
    },
    main: main
  } 
  return fullResponse
}

module.exports.resCraft = resCraft