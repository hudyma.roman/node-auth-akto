const Joi = require("@hapi/joi");

const validRegister = (reqBody) => {
  const schema = Joi.object({
    name: Joi.string().min(3).required(),
    email: Joi.string().min(6).required().email(),
    password: Joi.string().min(6).required(),
  });
  return schema.validate(reqBody);
};
const validLogin = (reqBody) => {
  const schema = Joi.object({
    email: Joi.string().min(6).required().email(),
    password: Joi.string().min(6).required(),
  });
  return schema.validate(reqBody);
};

module.exports.validRegister = validRegister;
module.exports.validLogin = validLogin;
