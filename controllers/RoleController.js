const { resCraft } = require("../helper/resCraft");
const RoleModel = require("../model/Role");

class RoleController {
  async read(req, res) {
    try {

      let all = await RoleModel.find();
      return res.send({
        userId: req.user,
        data: all,
      });

    } catch (e) {
      return res.status(500).send(resCraft("error", e));
    }
  }
  async create(req, res) {
    try {

      let name = req.body.name.toLowerCase()
      // Duplicate Role Validation
      const roleExist = await RoleModel.findOne({ name });
      if (roleExist) return res.send("Role alredy exist");
      
      const role = new RoleModel({name});

      const savedItem = await role.save();
      return res.send(savedItem);

    } catch (e) {
      return res.status(500).send(resCraft("error", e));
    }
  }
}

module.exports = new RoleController();

