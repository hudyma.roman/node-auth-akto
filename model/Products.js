const mongoose = require("mongoose");


const productSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  img: {
    path: {
      type: String,
      default: `public/img/no-image.svg`
    },
    title: {
      type: String,
      default: `no-image.svg`
    },
  },
  count: {
    type: Number,
    required: true,
  },
  price: {
    type: Number,
    required: true,
  },
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Users',
    required: true,
  },
  date: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Products", productSchema);
